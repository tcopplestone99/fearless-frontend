window.addEventListener('DOMContentLoaded', async () => {
  const selectTag = document.getElementById('conference');

  const url = 'http://localhost:8000/api/conferences/';
  const response = await fetch(url);
  if (response.ok) {
      const data = await response.json();

      for (let conference of data.conferences) {
          const option = document.createElement('option');
          option.value = conference.href;
          option.innerHTML = conference.name;
          selectTag.appendChild(option);
      }

      // Here, add the 'd-none' class to the loading icon
      let loading = document.getElementById("loading-conference-spinner");
      loading.classList.add("d-none");

      // Here, remove the 'd-none' class from the select tag
      let conferenceList = document.getElementById("conference");
      conferenceList.classList.remove("d-none");

  }
  const formTag = document.getElementById('create-attendee-form');
  formTag.addEventListener('submit', async event => {
      event.preventDefault();
      const formData = new FormData(formTag);
      const json = JSON.stringify(Object.fromEntries(formData));
      const attendeesURL = 'http://localhost:8001/api/attendees/';
      const fetchConfig = {
          method: "post",
          body: json,
          headers: {
              'Content-Type': 'application/json',
          },
      };
      const response = await fetch(attendeesURL, fetchConfig);
      if (response.ok) {
          formTag.reset();
          formTag.classList.add("d-none");
          let successMessage = document.getElementById("success-message");
          successMessage.classList.remove("d-none");
      }
  });
});
